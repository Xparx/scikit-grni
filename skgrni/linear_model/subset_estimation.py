import numpy as _np
import pandas as _pd
# from sklearn.linear_model import LinearRegression
# from sklearn.linear_model import LinearModel
from sklearn.utils import check_X_y
from sklearn.utils.validation import _is_arraylike as is_arraylike, check_is_fitted
from skgrni.utils import tol as _tol
# import functools as _functools


def _select_variables_from_prior(prior, tol=_tol):

    selected_v = _np.abs(prior) > tol
    return _np.array(selected_v)


def extract_variables_from_prior(X, prior, tol=_tol):

    selected = _select_variables_from_prior(prior)
    if not selected.any():
        X_v = _np.zeros(X.shape)
    else:
        X_v = X[:, selected]

    return X_v


def fill_in_from_prior(v, prior, tol=_tol):

    selected = _np.abs(prior) > tol

    tmp = _np.zeros(prior.shape)
    if selected.any():
        tmp[selected] = v

    return tmp


def subset_wrapper(cls):

    class SubsetEstimator(cls):
        """Documentation for SubsetEstimator"""
        def __init__(self, root_estimator, **super_params):

            super().__init__(**super_params)
            self.root_estimator = root_estimator

        def fit(self, X, y):

            if not is_arraylike(self.root_estimator):
                msg = "Estimator, %(name)s, must contain a fitted estimator."

                check_is_fitted(self.root_estimator, msg=msg)

                subset_coef = self.root_estimator.coef_

            else:
                subset_coef = self.root_estimator

            X, y = check_X_y(X, y, accept_sparse=['csr', 'csc', 'coo'], y_numeric=True, multi_output=True)

            X_v = extract_variables_from_prior(X, subset_coef)

            super().fit(X_v, y)

            self.coef_ = fill_in_from_prior(self.coef_.copy(), subset_coef).copy()

    return SubsetEstimator
