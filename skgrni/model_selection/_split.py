import numpy as _np
import inspect
# import pandas as _pd
from sklearn.model_selection import BaseCrossValidator
# from sklearn.model_selection import train_test_split
from sklearn.model_selection import LeaveOneOut
from sklearn.model_selection import PredefinedSplit
from sklearn.utils.validation import _is_arraylike as is_arraylike
from ..utils import svd as _svd
from ._theory import eta, _include


class CVFilter(BaseCrossValidator):
    """Cross validation leave out filter
    """

    def __init__(self, CV=LeaveOneOut, norm=1, reverse=False, threshold=None, *args, **kwargs):

        if inspect.isclass(CV):
            self.CV = CV(*args, **kwargs)
        else:
            self.CV = CV

        self.reverse = reverse
        self.norm = norm

        if threshold is not None:
            if not is_arraylike(threshold):
                threshold = [threshold]
            else:
                threshold = threshold
        else:
            threshold = None

        self.threshold = threshold

    def __getattribute__(self, s):
        """
        this is called whenever any attribute of a CVFilter object is accessed. This function first tries to
        get the attribute off CVFilter. If it fails then it tries to fetch the attribute from self.CV (an
        instance of the decorated class). If it manages to fetch the attribute from self.CV, and
        the attribute is an instance method then `time_this` is applied.
        """
        try:
            x = super().__getattribute__(s)
        except AttributeError:
            pass
        else:
            return x

        x = self.CV.__getattribute__(s)

        return x

    def get_n_splits(self, X=None, y=None, **kwargs):
        """Returns the number of splitting iterations in the supplied cross-validator.
        This might not coincide with the actual number of filtered splits which may be less.
        """

        return self.CV.get_n_splits(X, y, **kwargs)

    def split(self, X, y=None):

        threshold = self.threshold

        if threshold is None:
            s = _svd(X, compute_uv=False)
            s = s[-1]
        else:
            s = threshold[0]

        sy = 0
        pe = 1
        if y is not None:
            if threshold is None:
                sy = _svd(y, compute_uv=False)
                sy = sy[-1]
            else:
                sy = threshold[-1]

        for train, test in self.CV.split(X, y):
            e = eta(X, test, self.norm)

            if y is not None:
                pe = eta(y, test, self.norm)

            if self.reverse:
                if (e <= s) + (pe <= sy) < 2:  # One or none can be true but not both.
                    yield train, test
            else:
                if (e >= s) and (pe >= sy):
                    yield train, test


def LOOCV_splitter(data):

    passed = [a and b for a, b in zip(_include(data.X.T), _include(data.P.T))]
    loi = _np.argwhere(passed).T[0]
    fold = _np.ones(data.shape)
    fold[passed] = loi
    fold[[not p for p in passed]] = -1

    # fold = range(data.M)
    splitter = PredefinedSplit(fold)

    return splitter


def cv_filter_splitter(X, y=None, splitter=LeaveOneOut(), reverse=False, **kwargs):

    s = _svd(X, compute_uv=False)
    s = s[-1]

    sy = 0
    pe = 1
    if y is not None:
        sy = _svd(y, compute_uv=False)
        sy = sy[-1]

    for train, test in splitter.split(X, y):
        e = eta(X, test)

        if y is not None:
            pe = eta(y, test)

        if reverse:
            if (e <= s) + (pe <= sy) < 2:
                yield train, test
        else:
            if (e >= s) and (pe >= sy):
                yield train, test
